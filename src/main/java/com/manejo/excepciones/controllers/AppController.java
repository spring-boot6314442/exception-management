package com.manejo.excepciones.controllers;

import com.manejo.excepciones.exceptions.UserNotFoundException;
import com.manejo.excepciones.models.domain.User;
import com.manejo.excepciones.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AppController {
    @Autowired
    private IUserService iUserService;

    @GetMapping("/app")
    public String valor() {
        int value = Integer.parseInt("20x");
        return "ok 200";
    }

    @GetMapping("/show/{id}")
    public User showId(@PathVariable(value = "id", name = "id", required = true) Long id) {
        User user = iUserService.findById(id).orElseThrow(()-> new UserNotFoundException());
        System.out.println(user.getId());
        return user;
    }

    @GetMapping("/all")
    public List<User> showAll() {
        return iUserService.findAll();
    }
}

package com.manejo.excepciones.services;

import com.manejo.excepciones.models.domain.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {
    List<User> findAll();
    Optional<User> findById(Long id);

}

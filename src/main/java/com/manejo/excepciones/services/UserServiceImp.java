package com.manejo.excepciones.services;

import com.manejo.excepciones.exceptions.UserNotFoundException;
import com.manejo.excepciones.models.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImp implements IUserService {
    @Autowired
    private List<User> users;

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public Optional<User> findById(Long id) {
        return users.stream().filter(usr-> usr.getId().equals(id)).findFirst();
    }
}

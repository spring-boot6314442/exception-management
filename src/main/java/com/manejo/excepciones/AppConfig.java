package com.manejo.excepciones;

import com.manejo.excepciones.models.domain.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Configuration
public class AppConfig {
    @Bean
    public List<User> users() {
        List<User> users = new ArrayList<>();
        users.add(new User(1L, "pepe", "pepe", new Date()));
        users.add(new User(2L, "pepe", "pepe", new Date()));
        users.add(new User(3L, "pepe", "pepe", new Date()));
        users.add(new User(4L, "pepe", "pepe", new Date()));
        users.add(new User(5L, "pepe", "pepe", new Date()));
        return users;
    }
}

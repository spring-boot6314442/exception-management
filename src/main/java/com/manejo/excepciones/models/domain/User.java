package com.manejo.excepciones.models.domain;

import java.util.Date;

public class User {
    private Long id;
    private String name;
    private String lastname;
    private Date date;
    private Role role;

    public User(Long id, String name, String lastname, Date date) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
